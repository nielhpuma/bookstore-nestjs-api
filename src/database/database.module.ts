import { Module } from '@nestjs/common';
import { databaseProvider } from './database.service';

@Module({
  providers: [],
  imports: [...databaseProvider],
  exports: [...databaseProvider],
})
export class DatabaseModule {}
